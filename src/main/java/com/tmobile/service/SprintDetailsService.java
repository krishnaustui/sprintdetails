package com.tmobile.service;

import com.tmobile.entity.Sprint;
import com.tmobile.model.SprintResponse;
import com.tmobile.repository.SprintDetailsRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SprintDetailsService {

    @Autowired
    SprintDetailsRepository repository;

    public List<SprintResponse> getAllSprintDetails() {
        List<Sprint> sprintDetails = repository.findAll();
        return getTransformedSprintDetails(sprintDetails);

    }

    private List<SprintResponse> getTransformedSprintDetails(List<Sprint> sprintDetails) {
        List<SprintResponse> response = new ArrayList<>();
        for (Sprint sprint : sprintDetails) {
            SprintResponse sprintResponse = new SprintResponse();
            BeanUtils.copyProperties(sprint, sprintResponse);
            response.add(sprintResponse);
        }
        return response;
    }

    public SprintResponse getDetailsBySprintId(Long sprintId) {
        Optional<Sprint> sprintDetails = repository.findById(sprintId);
        SprintResponse response = new SprintResponse();
        if (sprintDetails.isPresent()) {
            BeanUtils.copyProperties(sprintDetails.get(), response);
            return response;
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sprint details does not exist with Id " + sprintId);
        }
    }
}
