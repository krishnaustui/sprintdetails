package com.tmobile.controller;

import com.tmobile.model.SprintResponse;
import com.tmobile.service.SprintDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/sprint")
public class SprintDetailsController {

    @Autowired
    SprintDetailsService service;

    @GetMapping("/getAll")
      public ResponseEntity<List<SprintResponse>> getAllSprintsDetails() {
        List<SprintResponse> response = service.getAllSprintDetails();
        return new ResponseEntity<>(
                response,
                HttpStatus.OK);
    }
    @GetMapping("/{sprintId}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<SprintResponse> getDetailsBySprintId(@PathVariable(required = true) Long sprintId) {
        SprintResponse response = service.getDetailsBySprintId(sprintId);
        return new ResponseEntity<>(
                response,
                HttpStatus.OK);
    }
}
