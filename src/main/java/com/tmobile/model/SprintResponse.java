package com.tmobile.model;

import lombok.Data;

import javax.persistence.Column;
import java.sql.Date;

@Data
public class SprintResponse {
    private Long sprints_id;
   
    private String name;

    private Date start_date;

    private Date end_date;

    private Date created_date;

    private Date updated_date;

    private String created_by;

    private String updated_by;
}
