package com.tmobile.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "sprints")
public class Sprint {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long sprints_id;
    @Column
    private String name;
    @Column
    private Date start_date;
    @Column
    private Date end_date;
    @Column
    private Date created_date;
    @Column
    private Date updated_date;
    @Column
    private String created_by;
    @Column
    private String updated_by;
}
