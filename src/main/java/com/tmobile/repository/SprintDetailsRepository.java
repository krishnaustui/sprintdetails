package com.tmobile.repository;

import com.tmobile.entity.Sprint;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SprintDetailsRepository extends JpaRepository<Sprint, Long> {
}
