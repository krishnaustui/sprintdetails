package com.tmobile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprintDetailsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SprintDetailsApplication.class, args);
	}

}
